-- Answers

#1
SELECT customerName FROM customers WHERE country = "Philippines";

#2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName LIKE "%LA Rochelle Gifts%";

#3
SELECT productName, MSRP FROM products WHERE productName LIKE "The Titanic%";

#4
SELECT firstName,lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

#5
SELECT customerName FROM customers WHERE state IS NULL;

#6
SELECT firstName,lastName,email from employees WHERE lastName = "Patterson" AND firstName = "Steve";

#7
SELECT customerName,country,creditLimit FROM customers WHERE country !="USA" AND creditLimit > 3000;

#8
SELECT customerName FROM customers WHERE customerName NOT LIKE '%a%';

#9
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

#10
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

#11
SELECT DISTINCT country FROM customers;

#12
SELECT DISTINCT status FROM orders;

#13
SELECT customerName, country FROM customers WHERE country IN ("USA","France") OR country = "Canada";

#14
SELECT employees.firstName AS first_name,employees.lastName as last_name,offices.city AS City
FROM employees JOIN offices
ON employees.officeCode = offices.officeCode;

#15
SELECT customers.customerName As Customer_Name
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

#16
SELECT products.productName as product_name,customers.customerName As customer_name
FROM customers JOIN orders
ON customers.customerNumber = orders.customerNumber
JOIN orderdetails
ON orders.orderNumber = orderdetails.orderNumber
JOIN products
ON orderdetails.productCode = products.productCode
WHERE customers.customerName = "Baane Mini Imports";

#17
SELECT employees.firstName as first_name, employees.lastName as last_name, customers.customerName as customer_name,
offices.country as Country
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
ON employees.officeCode = offices.officeCode;

#18
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

#19
SELECT productName, MAX(MSRP) FROM products GROUP BY productName ORDER BY MAX(MSRP);

#20
SELECT COUNT(country)AS Number_per_Country FROM customers WHERE country = "UK";

#21
SELECT productName,productLine,COUNT(productLine) AS Number_per_ProductLine
FROM products 
GROUP BY productLine;

#22
SELECT customerName,COUNT(salesRepEmployeeNumber) AS Number_served_by_employee
FROM customers GROUP BY salesRepEmployeeNumber;

#23
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;